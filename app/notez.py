#!/usr/bin/env python
# -*- coding: utf-8 -*-
import web
import lib.shared
import lib.diyenc
import lib.data
import lib.helpers

db = lib.shared.Globals.db
conf = lib.shared.Globals.conf
helpers = lib.helpers.helpersCatalog()
render = web.template.render(conf['templates'], base='layout', globals=helpers)	
urls = (
	'/login', 'Login',
	'/logout', 'Logout',
	'/trades/?(.*)', 'Trades',
	'/cryptos/?(.*)', 'Cryptos',
	'/streams/?(.*)', 'Streams',
	'/delete/(\d+)', 'Delete',
	'/edit/(\d+)', 'Edit',
	'/view/(\d+)', 'View',
	'/search/', 'Search',
	'/(.*)', 'Index',
)
app = web.application(urls, globals())
if web.config.get('_session') is None:
    session = web.session.Session(app, web.session.DiskStore(conf['sessions']))
    web.config._session = session
else:
    session = web.config._session

def auth():
	return session.get('logged_in')

class Streams:
   	streams = {
		'bloomberg': 'http://cdn3.videos.bloomberg.com/btv/us/master.m3u8'
   	}
	def GET(self, channel = None):
		if not channel:
			channel = 'bloomberg'
		data = [channel, self.streams]
		return render.streams(data)


class Trades:   
	def GET(self, tool = None):
		if not tool:
			tool = 'daily calendar'
		data = [tool]
		return render.trades(data)

class Cryptos:
	def default_tool(self):
		return 'most profitable'
	
	def tools(self):
		return [
			'most profitable'
		]

	def prepareTool(self, tool):
		return []

	def GET(self, tool = None):
		if not tool:
			tool = self.default_tool()
		toolData = self.prepareTool(tool)
		data = [tool, self.tools(), toolData]
		return render.cryptos(data)


class Index:
	def GET(self, search = None):
		if auth():
			form = lib.data.dataForm()			
			view = [form, lib.data.getData(search), lib.data.getTags()]
			return render.index(view)
		else:
			raise web.seeother('/login')	

	def POST(self, search = None):
		form = lib.data.dataForm() 
		if form.validates():
			tag = ''
			if(form.d.tag):
				tag = form.d.tag
			if(form.d.new_tag):
				tag = form.d.new_tag				
			db.insert('data',
				title = form.d.title, 
				content = lib.diyenc.encrypt(form.d.content.encode('utf-8'), conf['enc_password']),
				created = lib.helpers.now(),
				tag = tag,
				type = form.d.type
			)
		raise web.seeother('/')

class View:
	def GET(self, id):
		if(auth()):
			entry = lib.data.getDataById(id)
			if(entry):
				return render.view([entry])
			raise web.seeother('/')
		else:
			raise web.seeother('/login')

class Delete:
	def GET(self, id):
		if(auth()):
			entry = lib.data.getDataById(id)
			if entry:
				db.delete('data', where='id='+id)
			raise web.seeother('/')
		else:
			raise web.seeother('/login')

class Edit:
	def GET(self, id):
		entry = lib.data.getDataById(id)
		if entry:
			form = lib.data.dataForm()
			form.fill(entry)
			data = [form, lib.data.getData(), lib.data.getTags()]        	
			return render.index(data)
		raise web.seeother('/')
	
	def POST(self, id):
		entry = lib.data.getDataById(id)
		form = lib.data.dataForm()
		if form.validates():
			tag = ''
			if(form.d.tag):
				tag = form.d.tag
			if(form.d.new_tag):
				tag = form.d.new_tag				
			db.update('data',
				where="id=$id",
				title = form.d.title, 
				content = lib.diyenc.encrypt(form.d.content.encode('utf-8'), conf['enc_password']),
				created = lib.helpers.now(),
				tag = tag,
				type = form.d.type,
			 	vars=locals()
			)
			raise web.seeother('/view/'+id)
		raise web.seeother('/')

class Search:
    def POST(self):
    	data = web.input().s.encode('utf-8')
    	raise web.seeother('/'+data)



class Login:
    login_form = web.form.Form( 
    	web.form.Textbox('username', web.form.notnull),
        web.form.Password('password', web.form.notnull),
        web.form.Button('Login'),
    )

    def GET(self):
        f = self.login_form()
        return render.login(f)

    def POST(self):
        if not self.login_form.validates():
            return render.login(self.login_form)
        username = self.login_form['username'].value
        password = self.login_form['password'].value
        if (username,password) in conf['allowed']:
            session.logged_in = True
            raise web.seeother('/')
        return render.login(self.login_form)


class Logout:
    def GET(self):
        session.logged_in = False
        raise web.seeother('/')

        
if __name__ == "__main__":
    app.run()