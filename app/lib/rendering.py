
def plain(entry, options = []):
	return '<p>'+entry.content.replace("\n", "<br>")+'</p>'

def chart(entry, options = []):	
	chart = """
	<div id="container" style="width:100%; height:400px;"></div>
	<script>
	$(function () { 
	    $('#container').highcharts({
	        chart: {
	            type: 'bar'
	        },
	        title: {
	            text: 'Fruit Consumption'
	        },
	        xAxis: {
	            categories: ['Apples', 'Bananas', 'Oranges']
	        },
	        yAxis: {
	            title: {
	                text: 'Fruit eaten'
	            }
	        },
	        series: [{
	        	name: 'Fruit',
	            data: [1, 45, 4]
	        }]
	    });
	});
	</script>
	
	"""
	return chart

