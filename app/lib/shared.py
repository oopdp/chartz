import web
import os
import sys

home = ''
os.environ["SCRIPT_NAME"] = home
os.environ["REAL_SCRIPT_NAME"] = home
web.config.debug = True

class Globals(object):
	conf = { 
		'templates': 'templates/',
		'db' : '../data/testdb',
		'enc_password' : 'password',
		'log_file' : '../data/log.txt',
		'allowed' : (
			('user','pass'),
		),
		'sessions': '../data/sessions',
	}
	db = web.database(dbn='sqlite', db=conf['db'])
	
def log(data, mode = 'w'):
	text_file = open(Globals.conf['log_file'], "a")
	text_file.write(data)
	text_file.write("\n")
	text_file.close()