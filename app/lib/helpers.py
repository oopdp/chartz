from datetime import date
import time
import rendering

def niceTime(timestamp, format='%d-%m-%Y'):
	d = date.fromtimestamp(timestamp)
	return d.strftime(format)

def now():
	return time.time()
	
def renderContent(entry, options = []):	
	everyLanguageIsBroken = {
		'': rendering.plain,
		'chart': rendering.chart
	}
	myRenderer = everyLanguageIsBroken[entry.type]
	return myRenderer(entry, options)

def hi():
	return "hi"


def helpersCatalog():
	return {
		'niceDate': niceTime,
		'html': renderContent 
	}
