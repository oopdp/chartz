import shared
import diyenc
import web

db = shared.Globals.db
conf = shared.Globals.conf

def getTags(filter = ''):
	tags = db.select('data', order='tag')
	tt = []
	tt.append('')
	for t in tags:
		if(t.tag not in tt):
			tt.append(t.tag)
	return (tt)

def getTypes(filter = ''):
	return ['', 'chart']


def getData(filter = ''):
	tag = 'tag/'
	if filter.find(tag) >= 0:
		filter = filter.replace(tag, '')
		return db.select('data', vars={'search':filter}, where="tag = $search")
	return db.select('data', vars={'search':'%'+filter+'%'}, where="title LIKE $search")

def getDataById(id):
	try:
		entry = db.select('data', where='id='+id)[0]
		entry.content = diyenc.decrypt(entry.content, conf['enc_password'])
		return entry
	except IndexError:
		return None

def dataForm():
	form = web.form.Form(
		web.form.Textbox('title', web.form.notnull, 
			size=30,
			description="Title:"
		),
		web.form.Dropdown('tag', getTags(),
			description="Tag:"
		),
		web.form.Dropdown('type', getTypes(),
			description="Type:"
		),
		web.form.Textbox('new_tag', 
			size=30,
			description="New Tag:"
		),
		web.form.Textarea('content', web.form.notnull, 
			size=30,
			description="Data:"
		),
		web.form.Button('Save')
	)
	return form

def installApp():
	tbs = db.query("SELECT name FROM sqlite_master WHERE type='table'");
	found = False
	for t in tbs:
		if t.name == 'data':
			found = True
			break
	if not found:
		init = "create table data (id integer primary key autoincrement, title text, tag text, type text, content text, created int, deleted int);"
		db.query(init)
	return True