#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest 
import notez

class AppEncryption(unittest.TestCase): 
	def testATest(self):
		self.assertTrue(True)
	
	def testAESCipher(self):
		data = "àXXXà.àòdvd,@[er"
		aes = notez.lib.diyenc.AESCipher('secret')
		encrypted = aes.encrypt(data)
		decrypted = aes.decrypt(encrypted)
		self.assertEqual(decrypted, data)
		self.assertNotEqual(encrypted, decrypted)

	def testEncrypt(self):
		data = "xasdasalàòàòààààà\uisdudsasaà"
		enc = notez.lib.diyenc.encrypt(data)
		dec = notez.lib.diyenc.decrypt(enc)
		self.assertEqual(dec, data)

	def testEncryptUTF8(self):
		data = "\xc3\xa0\xc3\xa0\xc3\xa0\xc3\xa0\xc3\xa0\xc3\xa0\xc3\xa0\xc3\xa0\xc2\xb0"
		enc = notez.lib.diyenc.encrypt(data)
		dec = notez.lib.diyenc.decrypt(enc)
		self.assertEqual(dec, data)	

class AppTest(unittest.TestCase):
	def testATest(self):
		#print notez.lib.shared.Globals.conf
		self.assertTrue(True)

	def testHi(self):
		self.assertEqual(notez.lib.helpers.hi(), 'hi')

class EntrySim:
    def __init__(self, content, type = ''):
        self.content = content

class HelpersTest(unittest.TestCase):
	def testRenderPlain(self):
		self.assertTrue(True)
		e = EntrySim('x')
		r = notez.lib.rendering.plain(e)
		self.assertEqual('<p>x</p>', r)


if __name__ == "__main__": 
	unittest.main() 
